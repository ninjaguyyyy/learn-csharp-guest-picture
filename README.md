## 📺 Giới thiệu
Phần mềm Window chức năng câu hỏi trắc ngiệm
## Chức năng **GuessPicture**  
    - Có 10 câu hỏi trắc nghiệm (ảnh -> 2 đáp án tương ứng)
    - Chọn đúng sẽ cộng điểm.
    - Click nút Next hoặc mỗi 30s hoặc chọn sai -> không có điểm 

`Demo: `<img src="https://res.cloudinary.com/dwuma83gt/image/upload/v1602299875/aa_zclrrd.png" alt="ảnh demo" width="400" height="250">
## 🤵 Thông tin sinh viên
`Họ tên` Nguyễn Hữu Chí  
`Mssv` 1712299  
`Email` nguyenhuuchi3006@gmail.com
## 👍 Các chức năng đã làm được
- Tạo bộ câu hỏi 30 câu, thực hiện 10 câu trong số đó.
- Cho phép người chơi chơi: chọn đúng cộng điểm, sai/hết giờ/next không có điểm.
- Không nhúng ảnh vào file .exe mà để ở bên ngoài.
## 👎 Các chức năng chưa làm được
**Không có.**
## 🎉 Các điểm đặc sắc
**Không có.**
## 💣 Các con đường bất hạnh
**Không có.**
## 💌 Điểm đề nghị
Số điểm đề nghị: **10** điểm
## 📌 Link Youtube demo
Link: https://youtu.be/Tau4imsEs7s