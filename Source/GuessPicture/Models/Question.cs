﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessPicture.Models
{
    class Question
    {
        public string imageURL;
        public List<Choice> choices = new List<Choice>();

        public Question(string imageURL, List<Choice> choices)
        {
            this.imageURL = imageURL;
            this.choices = choices;
        }
    } 
}
