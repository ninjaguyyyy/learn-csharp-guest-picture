﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessPicture.Models
{
    class Choice
    {
        public string content;
        public bool isTrue;

        public Choice(string content, bool isTrue = false)
        {
            this.content = content;
            this.isTrue = isTrue;
        }
    }
}
