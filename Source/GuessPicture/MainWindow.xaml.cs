﻿using GuessPicture.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GuessPicture
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private int count = 30;
        private int countQuestion = 0;
        private int result = 0;
        private System.Timers.Timer countTimer;
        private System.Timers.Timer changeQuestionTimer;
        private Random _rng = new Random();
        private List<Question> _questions = new List<Question>() {
            new Question(
                "q_01.jpg",
                new List<Choice>() { 
                    new Choice("Mai Siêu Phong", true), 
                    new Choice("Quách Tương") 
                } 
            ),
            new Question(
                "q_02.jpg",
                new List<Choice>() {
                    new Choice("Hoàng Dung", true),
                    new Choice("Triệu Mẫn")
                }
            ),
            new Question(
                "q_03.jpg",
                new List<Choice>() {
                    new Choice("Tiểu Long Nữ", true),
                    new Choice("Quách Tương")
                }
            ),
            new Question(
                "q_04.jpg",
                new List<Choice>() {
                    new Choice("Triệu Mẫn", true),
                    new Choice("Chu Chỉ Nhược")
                }
            ),
            new Question(
                "q_05.jpg",
                new List<Choice>() {
                    new Choice("Quách Tương", true),
                    new Choice("Quách Phù")
                }
            ),
            new Question(
                "q_06.jpg",
                new List<Choice>() {
                    new Choice("Lý Mạc Sầu", true),
                    new Choice("Hoàng Dung")
                }
            ),
            new Question(
                "q_07.jpg",
                new List<Choice>() {
                    new Choice("Mục Niệm Từ", true),
                    new Choice("Mục Liên Từ")
                }
            ),
            new Question(
                "q_08.jpg",
                new List<Choice>() {
                    new Choice("Quách Phù", true),
                    new Choice("Anh Cô")
                }
            ),
            new Question(
                "q_09.jpg",
                new List<Choice>() {
                    new Choice("Chu Chỉ Nhược", true),
                    new Choice("Ân Ly")
                }
            ),
            new Question(
                "q_10.jpg",
                new List<Choice>() {
                    new Choice("Bát Hối", true),
                    new Choice("Ân Ly")
                }
            ),
            new Question(
                "q_11.jpg",
                new List<Choice>() {
                    new Choice("Puka", true),
                    new Choice("Lan Ngọc")
                }
            ),
            new Question(
                "q_12.png",
                new List<Choice>() {
                    new Choice("Nancy", true),
                    new Choice("Soyoen")
                }
            ),
            new Question(
                "q_13.jpg",
                new List<Choice>() {
                    new Choice("Ngọc Trinh", true),
                    new Choice("Phương Trinh")
                }
            ),
            new Question(
                "q_14.jpg",
                new List<Choice>() {
                    new Choice("Ngô Thanh Vân", true),
                    new Choice("Ốc Thanh Vân")
                }
            ),
            new Question(
                "q_15.jpg",
                new List<Choice>() {
                    new Choice("Lan Ngọc", true),
                    new Choice("Hương Giang")
                }
            ),
            new Question(
                "q_16.png",
                new List<Choice>() {
                    new Choice("Lâm Vỹ Dạ", true),
                    new Choice("Diệu Nhi")
                }
            ),
            new Question(
                "q_17.jpg",
                new List<Choice>() {
                    new Choice("Minh Hằng", true),
                    new Choice("Ngọc Hân")
                }
            ),
            new Question(
                "q_18.jpg",
                new List<Choice>() {
                    new Choice("Hậu duệ mặt trời", true),
                    new Choice("Diên Hy công lược")
                }
            ),
            new Question(
                "q_19.jpg",
                new List<Choice>() {
                    new Choice("Hậu duệ mặt trời", true),
                    new Choice("Gạo nếp gạo tẻ")
                }
            ),
            new Question(
                "q_20.jpg",
                new List<Choice>() {
                    new Choice("Tây du ký", true),
                    new Choice("Diên Hy công lược")
                }
            ),
            new Question(
                "q_21.png",
                new List<Choice>() {
                    new Choice("Ngọc Lan", true),
                    new Choice("Ngọc Loan")
                }
            ),
            new Question(
                "q_22.png",
                new List<Choice>() {
                    new Choice("Jumong", true),
                    new Choice("Ngôi nhà hạnh phúc")
                }
            ),
            new Question(
                "q_23.jpg",
                new List<Choice>() {
                    new Choice("Sam", true),
                    new Choice("Diệu Nhi")
                }
            ),
            new Question(
                "q_24.jpg",
                new List<Choice>() {
                    new Choice("Nhã Phương", true),
                    new Choice("Trường Giang")
                }
            ),
            new Question(
                "q_25.jpg",
                new List<Choice>() {
                    new Choice("Hương Giang", true),
                    new Choice("Khánh Vân")
                }
            ),
            new Question(
                "q_26.jpg",
                new List<Choice>() {
                    new Choice("Diễm My", true),
                    new Choice("Phương Trinh")
                }
            ),
            new Question(
                "q_27.jpg",
                new List<Choice>() {
                    new Choice("Vân Trang", true),
                    new Choice("Ngọc Trang")
                }
            ),
            new Question(
                "q_28.jpg",
                new List<Choice>() {
                    new Choice("Phương Trinh", true),
                    new Choice("Hari Won")
                }
            ),
            new Question(
                "q_29.jpg",
                new List<Choice>() {
                    new Choice("Chi Pu", true),
                    new Choice("Puka")
                }
            ),
            new Question(
                "q_30.jpg",
                new List<Choice>() {
                    new Choice("Anh Thư", true),
                    new Choice("Thanh Vân")
                }
            ),
        };

        public MainWindow()
        {
            countTimer = new System.Timers.Timer(1000);
            countTimer.Elapsed += countTimer_Elapsed;
            changeQuestionTimer = new System.Timers.Timer(30000);
            changeQuestionTimer.Elapsed += changeQuestionTimer_Elapsed;

            InitializeComponent();

            countTimer.Start();
            changeQuestionTimer.Start();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ChangeQuestion();
        }

        private void changeQuestionTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            
            Dispatcher.Invoke(() =>
            {
                ChangeQuestion();
                
            });
            throw new NotImplementedException();
        }

        private void countTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            
            count--;
            Dispatcher.Invoke(() =>
            {
                this.countLabel.Content = $"{count}";
            });
            
            throw new NotImplementedException();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
            ChangeQuestion();
        }

        public void ChangeQuestion()
        {
            count = 30;
            changeQuestionTimer.Stop();
            changeQuestionTimer.Start();
            countQuestion++;
            if (countQuestion == 11)
            {
                changeQuestionTimer.Stop();
                countTimer.Stop();
                MessageBox.Show($"Chúc mừng bạn, số điểm của bạn là {result}", "Thông báo");
                return;
            }
            var folder = AppDomain.CurrentDomain.BaseDirectory;
            var imagePath = $"{folder}Images\\{_questions[countQuestion - 1].imageURL}";
            questionImage.Source = new BitmapImage(new Uri(imagePath, UriKind.Absolute));

            questionLabel.Content = countQuestion;
            
            // random position anwsers
            int random = _rng.Next(2);
            
            choiceAButton.Content = _questions[countQuestion - 1].choices[random].content;
            choiceAButton.DataContext = _questions[countQuestion - 1].choices[random].isTrue;
            choiceBButton.Content = _questions[countQuestion - 1].choices[Math.Abs(random - 1)].content;
            choiceBButton.DataContext = _questions[countQuestion - 1].choices[Math.Abs(random - 1)].isTrue;
           
        }

        private void choiceAButton_Click(object sender, RoutedEventArgs e)
        {
            handleChoice(choiceAButton.DataContext.ToString());

        }

        private void choiceBButton_Click(object sender, RoutedEventArgs e)
        {
            handleChoice(choiceBButton.DataContext.ToString());

        }

        private void handleChoice(string dataReceived)
        {
            if (dataReceived == "True")
            {
                MessageBox.Show("Đúng", "Thông báo");
                result++;
                resultLabel.Content = result;
            }
            else
            {
                MessageBox.Show("Sai", "Thông báo");
            }
            ChangeQuestion();
        }
    }
}
